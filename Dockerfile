FROM node:16.15-alpine AS deps
ENV PORT=3000

RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json ./
RUN yarn install

FROM node:16.15-alpine AS runner
ENV NODE_ENV production

WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

EXPOSE ${PORT}

CMD ["npm", "start"]