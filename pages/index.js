import Head from 'next/head';
import styles from '../styles/Home.module.css';
import useSWR from 'swr';

const fetcher = (...args) => fetch(...args).then((res) => res.json());

export default function Home() {
  const { data: frontInfo, error } = useSWR('/api/hostinfo', fetcher);
  const { data: user, error: userError } = useSWR(
    `${process.env.NEXT_PUBLIC_BACKEND_URL}/users/1`,
    fetcher
  );
  const { data: backInfo, error: backInfoError } = useSWR(
    `${process.env.NEXT_PUBLIC_BACKEND_URL}/hostinfo`,
    fetcher
  );

  if (error) return <div>Failed to load</div>;
  if (!frontInfo) return <div>Loading...</div>;

  return (
    <div className={styles.container}>
      <Head>
        <title>WhoAmI</title>
        <meta name="description" content="Przykladowy projekt frontend'owy" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Cześć
          {!user || !user.success ? '' : `, ${user.data.firstName}!`}
        </h1>

        <div className={styles.grid}>
          <div className={styles.card}>
            <h2>Frontend</h2>
            <ul className={styles.list}>
              <li>
                <span>Hostname:</span> {frontInfo.data.hostname}
              </li>
              <li>
                <span>Platfrom:</span> {frontInfo.data.platform}
              </li>
              <li>
                <span>Release:</span> {frontInfo.data.release}
              </li>
              <li>
                <span>Arch:</span> {frontInfo.data.arch}
              </li>
            </ul>
          </div>

          <div className={styles.card}>
            <h2>Backend</h2>
            <ul className={styles.list}>
              <li>
                <span>Status:</span>{' '}
                {!backInfoError && !userError ? (
                  <span className={styles.statusOk}>OK</span>
                ) : (
                  <span className={styles.statusFail}>Fail</span>
                )}
              </li>
              {!backInfo ? (
                ''
              ) : (
                <>
                  <li>
                    <span>Hostname:</span> {backInfo.data.hostname}
                  </li>
                  <li>
                    <span>Platfrom:</span> {backInfo.data.platform}
                  </li>
                  <li>
                    <span>Release:</span> {backInfo.data.release}
                  </li>
                  <li>
                    <span>Arch:</span> {backInfo.data.arch}
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
      </main>
    </div>
  );
}
