// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import os from 'os';

export default function handler(req, res) {
  res.status(200).json({
    success: true,
    data: {
      hostname: os.hostname(),
      platform: os.platform(),
      release: os.release(),
      arch: os.arch(),
    },
  });
}
